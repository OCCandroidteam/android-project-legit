package com.garrett.androidproject;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import org.w3c.dom.Text;


public class HomePage extends Activity {

    Button protBTN;
    Button healthBTN;
    Button energyBTN;
    Button logoutBTN;
    TextView protDescTV;
    TextView healthDescTV;
    TextView energyDescTV;
    TextView homePageTitleTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        protBTN = (Button) findViewById(R.id.protButton);
        healthBTN = (Button) findViewById(R.id.healthBtn);
        //energyBTN = (Button) findViewById(R.id.energyMgmtBtn);
        //logoutBTN = (Button) findViewById(R.id.loginButton);
        protDescTV = (TextView) findViewById(R.id.protDescTV);
        healthDescTV = (TextView) findViewById(R.id.healthTV);
        energyDescTV = (TextView) findViewById(R.id.energyMgmtTV);
        homePageTitleTV = (TextView) findViewById(R.id.homePageTitle);

        final Button protBTN = (Button) findViewById(R.id.protButton);
        protBTN.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent intent = new Intent(HomePage.this, protActivity.class);
                HomePage.this.startActivity(intent);

                }

        }
        );

        final Button healthBTN = (Button) findViewById(R.id.healthBtn);
        healthBTN.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent intent = new Intent(HomePage.this, healthActivity.class);
                HomePage.this.startActivity(intent);


            }
        });

        final Button energyBTN = (Button) findViewById(R.id.energyMgmtBtn);
        energyBTN.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent intent = new Intent(HomePage.this, energyActivity.class);
                HomePage.this.startActivity(intent);

            }
        });

        final Button logoutBTN = (Button) findViewById(R.id.logoutBtn);
        logoutBTN.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                finish();
            }
        });
    }
}
