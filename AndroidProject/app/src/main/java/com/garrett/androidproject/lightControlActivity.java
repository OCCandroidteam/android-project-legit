package com.garrett.androidproject;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Created by Emrys on 4/23/2017.
 */

public class lightControlActivity extends Activity {

    TextView lightControlTitle;
    TextView floor1Label;
    TextView floor2Label;
    Switch livingRoomSw;
    Switch diningRoomSw;
    Switch kitchenSw;
    Switch bath1Sw;
    Switch basementSw;
    Switch garageSw;
    Switch bedroom1Sw;
    Switch bedroom2Sw;
    Switch bedroom3Sw;
    Switch bath2Sw;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lightcontrol);
        lightControlTitle = (TextView) findViewById(R.id.lightControlTitle);
        floor1Label = (TextView) findViewById(R.id.floor1Label);
        floor2Label = (TextView) findViewById(R.id.floor2Label);
        livingRoomSw = (Switch) findViewById(R.id.livingRoomLightSwitch);
        diningRoomSw = (Switch) findViewById(R.id.diningRoomLightSwitch);
        kitchenSw = (Switch) findViewById(R.id.kitchenLightSwitch);
        bath1Sw = (Switch) findViewById(R.id.floor1BathLightSwitch);
        basementSw = (Switch) findViewById(R.id.basementLightSwitch);
        garageSw = (Switch) findViewById(R.id.garageLightSwitch);
        bedroom1Sw = (Switch) findViewById(R.id.bedroom1LightSwitch);
        bedroom2Sw = (Switch) findViewById(R.id.bedroom2LightSwitch);
        bedroom3Sw = (Switch) findViewById(R.id.bedroom3LightSwitch);
        bath2Sw = (Switch) findViewById(R.id.floor2BathLightSwitch);


    }
}
