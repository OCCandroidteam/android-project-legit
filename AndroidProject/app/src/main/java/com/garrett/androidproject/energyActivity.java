package com.garrett.androidproject;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */


public class energyActivity extends Activity {

    TextView energyTitle;
    Button tempControlBtn;
    Button lightControlBtn;
    Button appControlBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.energyfragment);



        final Button tempControlBtn = (Button) findViewById(R.id.tempControlButton);
        tempControlBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent intent = new Intent(energyActivity.this, tempControlActivity.class);
                energyActivity.this.startActivity(intent);

            }
        });

        final Button energyBTN = (Button) findViewById(R.id.lightControlButton);
        energyBTN.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent intent = new Intent(energyActivity.this, lightControlActivity.class);
                energyActivity.this.startActivity(intent);

            }
        });

        final Button logoutBTN = (Button) findViewById(R.id.applianceControlButton);
        logoutBTN.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Intent intent = new Intent(energyActivity.this, applianceControlActivity.class);
                energyActivity.this.startActivity(intent);

            }
        });

    }


}




